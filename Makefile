all:compile
	

pedantic:
	gcc -fno-stack-protector -g -o p2 p2.c -pedantic
	
compile:
	gcc -fno-stack-protector -g -o p2 p2.c
	
run:
	./p2

sal:
	./p2 > sal
