#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
    char buffer[256];
    int source, target, count;


    if(argc != 3)
    {
        printf("Copiator buffered byte by byte highly innefficient file copier.\nUsage: copiator source copy-name\n");
    exit(0);
    }
    else if((source = open(argv[1], O_RDONLY)) == -1)
    {
        perror("Error opening source file:");
        exit(-1);
    }
    else if ((target = open(argv[2], O_WRONLY | O_TRUNC | O_CREAT, 0664)) == -1)
    {
        perror("Error creating the copy-to file");
        close(source);
        exit(-2);
    }

    while( (count =  read(source, buffer, 256)) != -1 )
    {
      if(write(target, buffer, count) != count)
      {
    perror("Fallo de escritura");
    close(source);
    close(target);
    exit(-3);
      } else if (count < 256)
    break;
    }

    printf("File copied successfully.\n");

    close(source);
    close(target);

    return 0;
}
