#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <aio.h>



#define MAX_BUFFER 1024*8

#define _FILE_OFFSET_BITS 64

/*ejecutar f1 hasta el final, hacer yield, copiar todo el stack a heap
, cambiar a f2 y repetir*/

//estados proceso
#define NEW 8
#define ZOMBIE -1
#define RUN 1
#define WAITING 0
//
#define MT_NEXT_FRAME 8
#define MT_RET_DIR 10
//tamano desde el array en la pila
#define MT_TAM_FRAME 12

#define Y_NEXT_FRAME 8
#define Y_RET_DIR 10
#define Y_TAM_FRAME 12

#define L_NEXT_FRAME 4
//OJITO
#define L_NEXT_RET_DIR 26

//CODIGO THREADS

#define MAX 10
#define MORE_TAM 9

unsigned long c;
//es un int porque solo se modifican 32bits
unsigned int _lNextFrame;
//es un int porque esta en la parte mas baja de memoria y ocupa menos de 32 bits
unsigned int *_yRetDir;

//solo para pruebas, quitar
int aux;

//para controlar crear y destruir
int count=0;
int o=0;

int currentPid = 0;
int currentThread = 0;
int numThreads = 0;
int threadsInit = 0;

int bloqueo = 0;

char message[1024];

void launch();

struct db_st{
	char *src;
	char *dst;
	int fdSrc;
	int fdDst;
	unsigned long totalBytes;
	unsigned long procesedBytes;
	unsigned long next;
} db_data;


//struct _thread_inf
typedef struct thread_inf
{
	long _stackSize;
	//unsigned int porque llega con 32 bits
	unsigned int * _stackPos;
	void * _RetDir;
	void * _RetFra;
	void * (*_func) (long);
	long _argv;
	int _id;
	int _state; //0:esperando / 1:ejecutando / -1:zombie / 8:nuevo
	long _retVal;
} thread_type;
//define struct _thread_inf *thread_inf;

thread_type threads[MAX];

struct aiocb aioSt[MAX];

char buffer[MAX][MAX_BUFFER];

long factorial(long f);


thread_type * findFreeElement()
{
	int i = currentPid;
	currentPid++;
	if (currentPid == MAX)
		return NULL;
	threads[i]._id=i;
	return &threads[i];	
}

void newProcElement()
{
	thread_type * p;
	bloqueo = 1;
	
	p = findFreeElement();
	if (p==NULL)
		{
			bloqueo = 0;
			return;
		}
	p->_state = NEW; //nuevo
	//memcpy
	numThreads++;
	bloqueo = 0;
}

long newThread(void (* func) (long),long *n)
{
	thread_type *p;
	//bloquear el reloj!
	bloqueo = 1;
	if (threadsInit >= MAX) exit(1);
	p = &threads[threadsInit++];
	
	p->_state = WAITING; 
	
	//desbloquear reloj
	
	p->_func = func;
	p->_argv = *n;

	bloqueo = 0;

}

/*
********************************
*/

void closeThread()
{
    //threads[currentThread]._state = WAITING;
    //if ((o) && (currentThread == 3))
    //{
    	threads[currentThread]._state = ZOMBIE;
    //	o=0;
	//}
    free(threads[currentThread]._stackPos);
    threads[currentThread]._stackSize = 0;
}
/*
********************************
*/

/**
 * funcion que escolle o thread para continuar a sua execucion
 */
void schedule()
{
	do
	{
		currentThread = (currentThread + 1) % numThreads;
	}
	while (threads[currentThread]._state == ZOMBIE);
}

void moreTam(long auxTam, int auxFirst)
{
	int i;
	int b[1]={8};
			//		printf("\tmoreTam\n");
	
	if (auxFirst)	
	{
		(&threads[currentThread])->_RetDir = b[MT_RET_DIR];
		// escollemos o seguinte thread a executar
		schedule();
	}
	
	
/*			printf("\t__CT:%d__\n",currentThread);
				//for (i=8;i<threads[currentThread]._stackSize+20;i++){
				for (i=0;i<MT_TAM_FRAME;i++){
					printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
				}
	*/
	if (threads[currentThread]._state == WAITING)
	{
	//VOLVER AL MAIN Y LANZAR YIELD
								//launch();
							/*    for (i=0;i<15;i++){
											printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
										}
										printf("a:%x\n",*_yRetDir);*/
	    b[MT_NEXT_FRAME] = _lNextFrame;//MT_NEXT_FRAME = 8
	    *_yRetDir =  *_yRetDir - 0xa;
								/*		printf("D:%x\n",*_yRetDir);
								for (i=0;i<15;i++){
											printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
										}*/
	    return;
	}
	
//	printf("\t%ld-%ld-%ld\n",threads[currentThread]._stackSize,auxTam,auxTam+MT_TAM_FRAME);
	
	if (threads[currentThread]._stackSize > auxTam+MT_TAM_FRAME)
	{
		int a[MORE_TAM];
		auxTam=auxTam+(sizeof(int)*(MORE_TAM+1));
		moreTam(auxTam,0);
	}
	
				
	
//				printf("\nmain:%p\tb:%p\ttam:%ld\tpos:%p\n\n", c, &b[4], threads[currentThread]._stackSize, c-threads[currentThread]._stackSize);
	
	
				//printf("retDir: %x\tretFra: %x\n ",threads[currentThread]._RetDir,threads[currentThread]._RetFra);
	memcpy(c-threads[currentThread]._stackSize, threads[currentThread]._stackPos, threads[currentThread]._stackSize); //MT_TAM_FRAME=12

	b[MT_RET_DIR]=threads[currentThread]._RetDir;
	b[MT_NEXT_FRAME]=threads[currentThread]._RetFra;
	/*
			for (i=0;i<((threads[currentThread]._stackSize/4)+MT_TAM_FRAME);i++){
				printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
			}
	
			printf("Se realiza el memcpy\n");*/
}

//FIN CODIGO THREADS



void yield()
{
	
	int a[1] = {0x101};
	int i, endAll=1;
	long tam = -1;
	thread_type *p;
	unsigned long z =  &tam;
	//FIXME mirar que es lo que hay al principo del yield
	
	//printf("Z: %p %ld\n", z,z);
	
	//FIXME panic
	if (bloqueo) return;//.....

	/*sprintf(message,"[%d]:pos:%p - tam:%ld \t",currentThread,a,threads[currentThread]._stackSize);
	write(0,message, strlen(message));*/

//	printf("\t__CT:%d__\n",currentThread);
	/*for (i=-5;i<25;i++)
	{
		printf("%d: [%x]: %x\n",i, &a[i],a[i]);
	}*/
		
	if (threads[currentThread]._state == WAITING)
	//while (threads[currentThread]._state == WAITING)
	{
        launch();
    }

	
	for (i=0;i<numThreads;i++)
	{
		if (!(threads[i]._state == ZOMBIE))
		{
			endAll=0;
			break;
		}
	}
	
	if (!endAll)
	{
// 		tam = c-z-(sizeof(int)*9);
		tam = c-z;
 		//tam/=sizeof(int); //porque va de 4B en 4B
		//printf("c: %p, z: %p tam: %ld\n", c,z, tam);

		p = &threads[currentThread];
		
		
		
		if (p->_stackSize == 0) //si su tamaño es 0, es la primera vez que se mete en yield y se debe copiar
		{//	printf("MALLOCK\n");
			//tam = c-z-(sizeof(int)*12);
			p->_stackPos = (unsigned long *)malloc (tam);
			if (p->_stackPos == NULL)
			{
				sprintf(message,"No hay suficiente memoria, abortando\n\0");
				write(0,message, strlen(message));
				exit(0);
			}
			//p._stackSize = tam;
		}
		
		//p->_stackSize = tam+sizeof(int);
		p->_stackSize = tam;
		
			//printf("CT:%d:tam:%ld\n",currentThread,p->_stackSize);
		
		p->_RetFra = &a[Y_NEXT_FRAME];
		p->_RetDir = a[Y_RET_DIR];
		//copia del stack al heap
/*		printf("\nYIELD\n");
		for (i=-2;i<(p->_stackSize/4);i++)
		{
			//printf("%d: [%p]: %p\n",i, &p->_stackPos[i],p->_stackPos[i]);
			printf("%d: [%p]: %p\n",i, &a[i],a[i]);
		}*/
		memcpy(p->_stackPos,z,p->_stackSize);
	/*	sprintf(message,"[%d]:pos:%p - tam:%ld \t\n",currentThread,(int)a,threads[currentThread]._stackSize);
	write(0,message, strlen(message));*/
/*	printf("\nYIELD2\n");
		for (i=0;i<(tam/4)+2;i++)
			printf("%d: [%p]: %p\n",i, &p->_stackPos[i],p->_stackPos[i]);
			//printf("%d: [%p]: %p\n",i, &a[i],a[i]);
	*/

				
		moreTam(p->_stackSize,1);
	}
}

void launch()
{
	thread_type * p;
	
	int l[1] = {6};
	
	//printf("\nlaunch\n");
/*	
			for (aux=0;aux<35;aux++)
				printf("%d: [%p]: %p\n",aux, &l[aux],l[aux]);
*/
	p = &threads[currentThread];
	p->_state = RUN;
	
	_lNextFrame=l[L_NEXT_FRAME];
	_yRetDir = &l[L_NEXT_RET_DIR];
	
	//lanza la función
	//p->_retVal = (p->_func) (p->_argv);
	//printf("antes lanzar\n");
	p->_retVal = (p->_func) (p->_argv);
	//printf("despues lanzar\n");
	
	//yield();
	
	closeThread();
	
	yield();
}
/*
struct aiocb {
               // The order of these fields is implementation-dependent 

               int             aio_fildes;     /* File descriptor 
               off_t           aio_offset;     /* File offset 
               volatile void  *aio_buf;        /* Location of buffer 
               size_t          aio_nbytes;     /* Length of transfer 
               int             aio_reqprio;    /* Request priority 
               struct sigevent aio_sigevent;   /* Notification method 
               int             aio_lio_opcode; /* Operation to be performed;
                                                  lio_listio() only */





void cpDB(long dont_touch_me){
	// funcion do thread
	//struct aiocb aioSt;
	unsigned long bytesRead;
						//				printf("cpDB\n");
	do{		//printf("restante:%d\t total:%d\tprocesados:%d\n",db_data.totalBytes-db_data.procesedBytes,db_data.totalBytes,db_data.procesedBytes);
// read aio
			//fichero del que se lee
		aioSt[currentThread].aio_fildes = db_data.fdSrc;
			//offset del fichero
		aioSt[currentThread].aio_offset = db_data.next;
			//donde se guardara la info leida
		aioSt[currentThread].aio_buf = buffer[currentThread];
			//tamanho del buffer
		aioSt[currentThread].aio_nbytes = MAX_BUFFER;
			//para asignar prioridad
		aioSt[currentThread].aio_reqprio = 10;
			//senhal que se recibe al completar la operacion sincrona
		aioSt[currentThread].aio_sigevent.sigev_notify = SIGEV_NONE;
		
		db_data.next += MAX_BUFFER;
		
		aio_read(&aioSt[currentThread]);
		//printf("fichero:%d\n",db_data.fdSrc);

		yield();

		//mientras siga leyendo hacer yield
		while (aio_error(&aioSt[currentThread]) == EINPROGRESS)
		{ //printf("cajo no\n");
			yield(); 
		}
		//comprobamos que no hubo un error al leer
		if(aio_error(&aioSt[currentThread]))
		{
			perror("aio_read");
			//exit(3);
		}
		
// write aio
			//guardamos los bytes leidos
		bytesRead = aio_return(&aioSt[currentThread]);
			//bytes que han sido leidos
		db_data.procesedBytes += bytesRead;
			//
		aioSt[currentThread].aio_fildes = db_data.fdDst;
			//bytes a escribir
			
		//	printf("bytesRead: %d\n",bytesRead);
		aioSt[currentThread].aio_nbytes = bytesRead;
//read(1,message,2);
		aio_write(&aioSt[currentThread]);

		yield();

		//mientras siga escribiendo hacer yield
		while (aio_error(&aioSt[currentThread]) == EINPROGRESS)
		{	//printf("cajo na\n");
			yield();
		}
		//comprobamos que no hubo un error al escribir
		if(aio_error(&aioSt[currentThread]))
		{
			perror("aio_write:");
			//exit(4);
		}
	} while(db_data.procesedBytes < db_data.totalBytes);// nos falten bytes

	// acabar thread
	//yield();
}



long double_buffering(char *src, char *dst){
	struct stat st;
	long var =0;
/*
struct db_st{
	char *src;
	char *dst;
	int fdSrc;
	int fdDst;
	unsigned long totalBytes;
	unsigned long procesedBytes;
	unsigned long next;
} db_data;*/
//printf("double buffering\n");
	// abrir ficheiros
	if ((db_data.fdSrc = open(src, O_RDONLY|O_NONBLOCK)) == -1){
		perror("Non se puido abrir o ficheiro fonte");
		exit(1);
	}
	
	if ((db_data.fdDst = open(dst, O_WRONLY|O_CREAT|O_NONBLOCK, 0664)) == -1){
		perror("Non se puido abrir o ficheiro destino");
		exit(2);
	}	
	
	//crear el fichero destino con el mismo tamaño que el fichero origen
	fstat(db_data.fdSrc,&st);
	ftruncate(db_data.fdDst, st.st_size);
	
	// inicializar db_data
	db_data.src = src;
	db_data.dst = dst;
	db_data.procesedBytes = 0L;
	db_data.next = 0L;
	db_data.totalBytes = st.st_size;
	
	//inicializamos dos threads
	newProcElement();
	newProcElement();

	//asignamos funcion a cada thread
	newThread(cpDB, &var);
	newThread(cpDB, &var);

	yield(); // aqui temos que lanzar os threads
	
	sprintf(message,"ACABADO. Total bytes copiados: %ld\n\0", db_data.procesedBytes);
	write(0,message,strlen(message));
	
	// pechar ficheiros
	close((int)(db_data.src));
	close((int)db_data.dst);

}







int checkArg(int argc, char *argv[]){
	struct stat st;
	int st_ret;

	if (argc < 3){
		return 0;
	}
	
	if ((st_ret = stat(argv[1], &st) == -1)){
		perror("fonte non existe");
		return 0;
	}
	
	return 1;
}

void printSyntax(){
	printf("erro sintaxe\n");
	//FIXME
}


int main(int argc, char **argv)
{
	int _main=0x88;
	int a[3] = {111,111,111};
	
	c =(unsigned long ) &(_main);

	if (!checkArg(argc, argv)){
		printSyntax();
	}
	
	double_buffering(argv[1], argv[2]);
	
		
/*
	yield();*/

	return(0);

}
