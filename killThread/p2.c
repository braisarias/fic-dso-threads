#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

/*ejecutar f1 hasta el final, hacer yield, copiar todo el stack a heap
, cambiar a f2 y repetir*/

//estados proceso
#define NEW 8
#define ZOMBIE -1
#define RUN 1
#define WAITING 0
//
#define MT_NEXT_FRAME 8
#define MT_RET_DIR 10
//tamano desde el array en la pila
#define MT_TAM_FRAME 12

#define Y_NEXT_FRAME 8
#define Y_RET_DIR 10
#define Y_TAM_FRAME 12

#define L_NEXT_FRAME 4
//OJITO
#define L_NEXT_RET_DIR 26

//CODIGO THREADS

#define MAX 10
#define MORE_TAM 9

unsigned long c;
//es un int porque solo se modifican 32bits
unsigned int _lNextFrame;
//es un int porque esta en la parte mas baja de memoria y ocupa menos de 32 bits
unsigned int *_yRetDir;

//solo para pruebas, quitar
int aux;

int currentPid = 0;
int currentThread = 0;
int numThreads = 0;
int threadsInit = 0;

int bloqueo = 0;

char message[1024];

void launch();

//struct _thread_inf
typedef struct thread_inf
{
	long _stackSize;
	//unsigned int porque llega con 32 bits
	unsigned int * _stackPos;
	void * _RetDir;
	void * _RetFra;
	void * (*_func) (long);
	long _argv;
	int _id;
	int _state; //0:esperando / 1:ejecutando / -1:zombie / 8:nuevo
	long _retVal;
} thread_type;
//define struct _thread_inf *thread_inf;

thread_type threads[MAX];


thread_type * findFreeElement()
{
	int i = currentPid;
	currentPid++;
	threads[i]._id=i;
	return &threads[i];	
}

void newProcElement()
{
	thread_type * p;
	bloqueo = 1;
	
	p = findFreeElement();
	
	p->_state = NEW; //nuevo
	//memcpy
	numThreads++;
	bloqueo = 0;
}

void newThread(long (* func)( long), long *n)
{
	thread_type *p;
	//bloquear el reloj!
	bloqueo = 1;
	if (threadsInit >= MAX) exit(1);
	p = &threads[threadsInit++];
	
	p->_state = WAITING; 
	
	//desbloquear reloj
	
	p->_func = func;
	p->_argv = *n;

	bloqueo = 0;

}

/*
********************************
*/

void closeThread()
{
    threads[currentThread]._state = WAITING;
    if (currentThread == 0)
    	threads[currentThread]._state = ZOMBIE;
    free(threads[currentThread]._stackPos);
    threads[currentThread]._stackSize = 0;
}
/*
********************************
*/

/**
 * funcion que escolle o thread para continuar a sua execucion
 */
void schedule()
{
	do
	{
		currentThread = (currentThread + 1) % numThreads;
	}
	while (threads[currentThread]._state == ZOMBIE);
}

void moreTam(long auxTam, int auxFirst)
{
	int i;
	int b[1]={8};
			//		printf("\tmoreTam\n");
	
	if (auxFirst)	
	{
		(&threads[currentThread])->_RetDir = b[MT_RET_DIR];
		// escollemos o seguinte thread a executar
		schedule();
	}
	
	
/*			printf("\t__CT:%d__\n",currentThread);
				//for (i=8;i<threads[currentThread]._stackSize+20;i++){
				for (i=0;i<MT_TAM_FRAME;i++){
					printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
				}
	*/
	if (threads[currentThread]._state == WAITING)
	{
	//VOLVER AL MAIN Y LANZAR YIELD
								//launch();
							/*    for (i=0;i<15;i++){
											printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
										}
										printf("a:%x\n",*_yRetDir);*/
	    b[MT_NEXT_FRAME] = _lNextFrame;//MT_NEXT_FRAME = 8
	    *_yRetDir =  *_yRetDir - 0xa;
								/*		printf("D:%x\n",*_yRetDir);
								for (i=0;i<15;i++){
											printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
										}*/
	    return;
	}
	
//	printf("\t%ld-%ld-%ld\n",threads[currentThread]._stackSize,auxTam,auxTam+MT_TAM_FRAME);
	
	if (threads[currentThread]._stackSize > auxTam+MT_TAM_FRAME)
	{
		int a[MORE_TAM];
		auxTam=auxTam+(sizeof(int)*(MORE_TAM+1));
		moreTam(auxTam,0);
	}
	
				
	
//				printf("\nmain:%p\tb:%p\ttam:%ld\tpos:%p\n\n", c, &b[4], threads[currentThread]._stackSize, c-threads[currentThread]._stackSize);
	
	
				//printf("retDir: %x\tretFra: %x\n ",threads[currentThread]._RetDir,threads[currentThread]._RetFra);
	memcpy(c-threads[currentThread]._stackSize, threads[currentThread]._stackPos, threads[currentThread]._stackSize); //MT_TAM_FRAME=12
	//creo que sobra
	//ahora creo que no sobra, pues cuando se hace recursivamente moreTam se apunta a otro lado
	b[MT_RET_DIR]=threads[currentThread]._RetDir;
	b[MT_NEXT_FRAME]=threads[currentThread]._RetFra;
	/*
			for (i=0;i<((threads[currentThread]._stackSize/4)+MT_TAM_FRAME);i++){
				printf("%d: [%p]: %17p\n",i, &b[i],b[i]);
			}
	
			printf("Se realiza el memcpy\n");*/
}

//FIN CODIGO THREADS



void yield()
{
	
	int a[1] = {0x101};
	int i, endAll=1;
	long tam = -1;
	thread_type *p;
	unsigned long z =  &tam;
	//FIXME mirar que es lo que hay al principo del yield
	
	//printf("Z: %p %ld\n", z,z);
	
	//FIXME panic
	if (bloqueo) return;//.....

	/*sprintf(message,"[%d]:pos:%p - tam:%ld \t",currentThread,a,threads[currentThread]._stackSize);
	write(0,message, strlen(message));*/

//	printf("\t__CT:%d__\n",currentThread);
	/*for (i=-5;i<25;i++)
	{
		printf("%d: [%x]: %x\n",i, &a[i],a[i]);
	}*/
		
	//if (threads[currentThread]._state == WAITING)
	while (threads[currentThread]._state == WAITING)
	{
        launch();
    }

	
	for (i=0;i<numThreads;i++)
	{
		if (!(threads[i]._state == ZOMBIE))
		{
			endAll=0;
			break;
		}
	}
	
	if (!endAll)
	{
// 		tam = c-z-(sizeof(int)*9);
		tam = c-z;
 		//tam/=sizeof(int); //porque va de 4B en 4B
		//printf("c: %p, z: %p tam: %ld\n", c,z, tam);

		p = &threads[currentThread];
		
		
		
		if (p->_stackSize == 0) //si su tamaño es 0, es la primera vez que se mete en yield y se debe copiar
		{//	printf("MALLOCK\n");
			//tam = c-z-(sizeof(int)*12);
			p->_stackPos = (unsigned long *)malloc (tam);
			if (p->_stackPos == NULL)
			{
				sprintf(message,"No hay suficiente memoria, abortando\n\0");
				write(0,message, strlen(message));
				exit(0);
			}
			//p._stackSize = tam;
		}
		
		//p->_stackSize = tam+sizeof(int);
		p->_stackSize = tam;
		
			//printf("CT:%d:tam:%ld\n",currentThread,p->_stackSize);
		
		p->_RetFra = &a[Y_NEXT_FRAME];
		p->_RetDir = a[Y_RET_DIR];
		//copia del stack al heap
/*		printf("\nYIELD\n");
		for (i=-2;i<(p->_stackSize/4);i++)
		{
			//printf("%d: [%p]: %p\n",i, &p->_stackPos[i],p->_stackPos[i]);
			printf("%d: [%p]: %p\n",i, &a[i],a[i]);
		}*/
		memcpy(p->_stackPos,z,p->_stackSize);
		sprintf(message,"[%d]:pos:%pM - tam:%ldM \t\n",currentThread,(int)a/1000000,threads[currentThread]._stackSize/1000000);
	write(0,message, strlen(message));
/*	printf("\nYIELD2\n");
		for (i=0;i<(tam/4)+2;i++)
			printf("%d: [%p]: %p\n",i, &p->_stackPos[i],p->_stackPos[i]);
			//printf("%d: [%p]: %p\n",i, &a[i],a[i]);
	*/

				
		moreTam(p->_stackSize,1);
	}
}

void launch()
{
	thread_type * p;
	
	int l[1] = {6};
	
/*	printf("\nlaunch\n");
	
			for (aux=0;aux<35;aux++)
				printf("%d: [%p]: %p\n",aux, &l[aux],l[aux]);
*/
	p = &threads[currentThread];
	p->_state = RUN;
	
	_lNextFrame=l[L_NEXT_FRAME];
	_yRetDir = &l[L_NEXT_RET_DIR];
	
	//lanza la función
	p->_retVal = (p->_func) (p->_argv);
	
	//yield();
	
	closeThread();
	
	//yield();
}

void slowly()
{
	int i;
	for (i=0;i<10000;i++)
		555525.5548465/666666.5789875*542.365/78932.58;
}



long factorial(long f)//8-19
{
	long aux = 1;
	/*sprintf(message,"\t[%d]fact: %ld\n",currentThread,f);
	write(0,message,strlen(message));*/
	//slowly();
	long a[10000];
	a[500]=7;
	if (f>0)
	{
		aux = f*factorial(f-1);
	}
	else
		yield();
	/*sprintf(message,"\t[%d]FACT{%ld}: %ld\n",currentThread,f,aux);
	write(0,message,strlen(message));*/
	slowly();
	//yield();
	
	return aux;
}



int main(int argc, char **argv)
{
	int _main=0x88;
	int a[3] = {111,111,111};
	long var1=2000;
	long var2=2000;
	long var3=5000;

	
	c =(unsigned long ) &(_main);
	
	//printf("MAIN %x %u\n", c, c);
	//juntar en uno que le pase el nº de threads y los cree!!
	newProcElement();
	newProcElement();
	newProcElement();



	newThread(factorial,&var1);
	newThread(factorial,&var2);
	newThread(factorial,&var3);

	yield();
	
	sprintf(message,"Thread %d:fact(%ld)= %ld\n\0",0,var1,threads[0]._retVal);
	write(0,message, strlen(message));
	sprintf(message,"Thread %d:fact(%ld)= %ld\n\0",1,var2,threads[1]._retVal);
	write(0,message, strlen(message));
	sprintf(message,"Thread %d:fact(%ld)= %ld\n\0",2,var3,threads[2]._retVal);
	write(0,message, strlen(message));

	return(0);

}
